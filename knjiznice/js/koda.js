
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}

function generirajVsePodatke() {
    for(var i = 0; i < 3; i++)
        generirajPodatke(i+1);
    $('#preberiPredlogoBolnika').text("");
    $('#preberiPredlogoBolnika').html("<option value=\"1,Janez,Novak,1990,Slovenija,185,80.00,0333abcd-c56c-4386-9697-16d4e1438d22\">Janez Novak</option><option value=\"2,Mike,Smith,1943,Amerika,190,175.50,5cdebdfa-0cd7-4d85-a1c1-dad97cee3da3\">Mike Smith</option><option value=\"3,Hiku,Matsune,2000,Japonska,155,55.00,54a6dd28-e357-4054-b363-c272b8b59449\">Hiku Matsune</option>");
}

function pridobiCifro() {
    $('#kreirajSporocilo').html("");
    return Number($('#preberiPredlogoBolnika').val().split(",")[0]);
}

/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
    // var vnos = $('#preberiPredlogoBolnika').val();
    // console.log(stPacienta + " " + typeof(stPacienta));
    var ehrId = "";
    switch(stPacienta){
        case 1:
            $("#kreirajSporocilo").html("</br>Janez Novak, rojen 1990, država: Slovenija, višina: 185cm, teža: 80.00kg");
            ehrId = "0333abcd-c56c-4386-9697-16d4e1438d22";
            kreiraj("Janez", "Novak", "1990", "Slovenija", "185", "80.00");
            $("#preberiObstojeciEHR").append("<option value = " + ehrId + ">" + ehrId + "</option>");
            break;
        case 2:
            ehrId = "5cdebdfa-0cd7-4d85-a1c1-dad97cee3da3";
            kreiraj("Mike", "Smith", "1943", "Amerika", "195", "175.50");
            $("#preberiObstojeciEHR").append("<option value = " + ehrId + ">" + ehrId + "</option>");
            $("#kreirajSporocilo").append("</br>Mike Smith, rojen 1953, država: Amerika, višina: 195cm, teža: 175.50kg");
            break;
        case 3:
            ehrId = "54a6dd28-e357-4054-b363-c272b8b59449";
            kreiraj("Hiku", "Matsune", "2000", "Japonska", "155", "155", "55.00");
            $("#preberiObstojeciEHR").append("<option value = " + ehrId + ">" + ehrId + "</option>");
            $("#kreirajSporocilo").append("</br>Hiku Matsune, rojen 2000, država: Japonska, višina: 155cm, teža: 55.00kg");
            break;
    }

    return ehrId;
}

function kreiraj(ime, priimek, leto, drzava, visina, teza) {
    sessionId = getSessionId();

    if (!ime || !priimek || !leto || !drzava || !visina || !teza ||
      ime.trim().length == 0 || priimek.trim().length == 0 || leto.trim().length == 0 ||
      drzava.trim().length == 0 || visina.trim().length == 0 || teza.trim().length == 0) {
        $("#kreirajSporocilo").html("<span class='obvestilo label " +
          "label-warning fade-in'>Prosim vnesite vse zahtevane podatke!</span>");
        // console.log("prazna polja");
        // console.log(" " + ime + " " + priimek + " " + leto + " " + drzava + " " + visina + " " + teza);
    } else {
        $.ajaxSetup({
            headers: {"Ehr-Session": sessionId}
        });
        $.ajax({
            url: baseUrl + "/ehr",
            type: 'POST',
            success: function (data) {
                // console.log("naj bi delal");
                var ehrId = data.ehrId;
                // var ehrId = id;
                var partyData = {
                    firstNames: ime,
                    lastNames: priimek,
                    yearOfBirth: leto,
                    country: drzava,
                    height: visina,
                    weight: teza,
                    partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
                };
                $.ajax({
                    url: baseUrl + "/demographics/party",
                    type: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify(partyData),
                    success: function (party) {
                        if (party.action == 'CREATE') {
                            $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
                            $("#preberiEHRid").val(ehrId);
                        }
                    },
                    error: function(err) {
                        $("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
                    }
                });
                
            }
        });
    }
}

function kreirajPodatkeOBolniku() {
    console.log("kliknen gubm za kreiranje podatkov");
    sessionId = getSessionId();
    
    
    var id = $("dodajVitalnoEHR").val();
    var ime = $("#kreirajIme").val();
    var priimek = $("#kreirajPriimek").val();
    var leto = $("#kreirajLeto").val();
    var drzava = $("#dodajDrzavo").val();
    var visina = $("#dodajVitalnoTelesnaVisina").val();
    var teza = $("dodajVitalnoTelesnaTeza").val();
    

    if (!id || !ime || !priimek || !leto || !drzava || !visina || !teza ||
      ime.trim().length == 0 || priimek.trim().length == 0 || leto.trim().length == 0 ||
      drzava.trim().length == 0 || visina.trim().length == 0 || teza.trim().length == 0) {
        $("#kreirajSporocilo").html("<span class='obvestilo label " +
          "label-warning fade-in'>Prosim vnesite vse zahtevane podatke!</span>");
        // console.log("prazna polja");
        // console.log(id + " " + ime + " " + priimek + " " + leto + " " + drzava + " " + visina + " " + teza);
    } else {
        $.ajaxSetup({
            headers: {"Ehr-Session": sessionId}
        });
        $.ajax({
            url: baseUrl + "/ehr",
            type: 'POST',
            success: function (data) {
                // console.log("naj bi delal");
                var ehrId = data.ehrId;
                // var ehrId = id;
                var partyData = {
                    firstNames: ime,
                    lastNames: priimek,
                    yearOfBirth: leto,
                    country: drzava,
                    height: visina,
                    weight: teza,
                    partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
                };
                $.ajax({
                    url: baseUrl + "/demographics/party",
                    type: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify(partyData),
                    success: function (party) {
                        if (party.action == 'CREATE') {
                            $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
                            $("#preberiEHRid").val(ehrId);
                        }
                    },
                    error: function(err) {
                        $("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
                    }
                });
                
                // dodej vnos
                $("#preberiEHRid").html("<option value = " + ehrId + ">" + ehrId + "</option>");
            }
        });
    }
}

$(document).ready(function() {
  /**
   * Napolni testne vrednosti (ime, priimek in datum rojstva) pri kreiranju
   * EHR zapisa za novega bolnika, ko uporabnik izbere vrednost iz
   * padajočega menuja (npr. Pujsa Pepa).
   */
      $('#preberiPredlogoBolnika').change(function() {
        $("#kreirajSporocilo").html("");
        var podatki = $('#preberiPredlogoBolnika').val().split(",");
        $("#dodajVitalnoEHR").val(podatki[7])
        $("#kreirajIme").val(podatki[1]);
        $("#kreirajPriimek").val(podatki[2]);
        $("#kreirajLeto").val(podatki[3]);
        $("#dodajDrzavo").val(podatki[4]);
        $("#dodajVitalnoTelesnaVisina").val(podatki[5]);
        $("#dodajVitalnoTelesnaTeza").val(podatki[6]);
    });
});